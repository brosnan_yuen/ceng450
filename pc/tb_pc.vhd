library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library vunit_lib;
context vunit_lib.vunit_context;


entity tb_pc is
  generic (runner_cfg : string);
end entity;

architecture tb of tb_pc is



component pc is
	Port ( 
		clk 			: in  STD_LOGIC;
		rst 			: in  STD_LOGIC;
		increment		: in  STD_LOGIC;
		update			: in  STD_LOGIC;
		update_program_counter	: in  STD_LOGIC_VECTOR (15 downto 0);
		program_counter		: out STD_LOGIC_VECTOR (15 downto 0)
	);
end component;



signal  rst, increment, update,clk : std_logic;
signal  program_counter, update_program_counter : std_logic_vector(15 downto 0);


begin
	mapping: pc port map(clk, rst, increment, update , update_program_counter, program_counter  );
	main : process
	begin
		test_runner_setup(runner, runner_cfg);
		report "Start testing PC";
		
	
		rst <= '1';
		increment <= '0';
		update <= '0';
		update_program_counter <= (others => '0');
			
		clk <= '0';
		wait for 5 ns;
		clk <= '1';
		wait for 5 ns;
			
		check(program_counter  = "0000000000000000" , "Test reset output" );
		
		rst <= '0';
		
		clk <= '0';
		wait for 5 ns;
		clk <= '1';
		wait for 5 ns;

		increment <= '0';
	

		clk <= '0';
		wait for 5 ns;
		clk <= '1';
		wait for 5 ns;

		increment <= '1';	

		
		clk <= '0';
		wait for 5 ns;
		clk <= '1';
		wait for 5 ns;

		increment <= '0';

		check(program_counter  = "0000000000000010" , "Test increment output" );

		
		clk <= '0';
		wait for 5 ns;
		clk <= '1';
		wait for 5 ns;

		increment <= '1';

		
		clk <= '0';
		wait for 5 ns;
		clk <= '1';
		wait for 5 ns;

		increment <= '0';


		check(program_counter  = "0000000000000100" , "Test increment output 2" );

		clk <= '0';
		wait for 5 ns;
		clk <= '1';
		wait for 5 ns;
		
		update_program_counter  <= "0000000100000000";

		update <= '1';

		clk <= '0';
		wait for 5 ns;
		clk <= '1';
		wait for 5 ns;

		check(program_counter  <= "0000000100000000" , "Test update output" );
		
		
		clk <= '0';
		wait for 5 ns;
		clk <= '1';
		wait for 5 ns;

		update_program_counter  <= "0000000100000100";


		clk <= '0';
		wait for 5 ns;
		clk <= '1';
		wait for 5 ns;


		update <= '0';

		clk <= '0';
		wait for 5 ns;
		clk <= '1';
		wait for 5 ns;



		check(program_counter  <= "0000000100000100" , "Test update output 2" );


		test_runner_cleanup(runner); -- Simulation ends here
	end process;
end architecture;
