LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity pc is
	Port ( 
		clk 			: in  STD_LOGIC;
		rst 			: in  STD_LOGIC;
		increment		: in  STD_LOGIC;
		update			: in  STD_LOGIC;
		update_program_counter	: in  STD_LOGIC_VECTOR (15 downto 0);
		program_counter		: out STD_LOGIC_VECTOR (15 downto 0)
	);
end pc;

architecture Behavioral of pc is
	signal counter	: STD_LOGIC_VECTOR (15 downto 0) := "0000000000000000";
begin

process(clk,rst,increment,update,update_program_counter,counter)
begin

	if (rising_edge(clk)) then 
		if (rst = '1') then --RESET
			counter <=  "0000000000000000";
		else
			if (update = '1') then --Update program counter
				counter <= update_program_counter;
			elsif (increment = '1') then
				counter  <= std_logic_vector( unsigned(counter(15 downto 0) ) + "10" ) ;
			end if;
		end if;
	end if;
	program_counter <= counter;
end process;

end Behavioral;
