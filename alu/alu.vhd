library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

 
entity alu is
    Port ( 
		clk : in std_logic; 
		rst : in std_logic;
		alu_mode : in std_logic_vector(2 downto 0);
		in0 : in std_logic_vector(15 downto 0);
		in1 : in std_logic_vector(15 downto 0);
		z_flag : out std_logic; 
		n_flag : out std_logic;
		result : out std_logic_vector(15 downto 0)
	);
end alu;

architecture Behavioral of alu is


begin
	process(clk,rst,alu_mode,in0,in1)
	begin
		if (rising_edge(clk)) then --Rising edge
			
			if (rst = '1') then --Reset
				z_flag <= '0';
				n_flag <= '0';
				result <= "0000000000000000";
			else
				case alu_mode is
					when "000" => --NOP 
						z_flag <= '0';
						n_flag <= '0';
						result <= "0000000000000000";
					when "001" => --ADDER 
						result <=  std_logic_vector(signed(in0(15 downto 0)) + signed(in1(15 downto 0)));
						z_flag <= '0';
						n_flag <= '0';	
					when "010" => --SUBTRACTOR
						result <=  std_logic_vector(signed(in0(15 downto 0)) - signed(in1(15 downto 0)));
						z_flag <= '0';
						n_flag <= '0';
					when "011" => --Multiplier
						result <=  std_logic_vector(unsigned(in0(7 downto 0)) * unsigned(in1(7 downto 0)));
						z_flag <= '0';
						n_flag <= '0';
					when "100" => --NAND
						result <=  in0 nand in1;
						z_flag <= '0';
						n_flag <= '0';
					when "101" => --Shift left
						result <=  std_logic_vector(shift_left(unsigned(in0), to_integer(unsigned(in1))));
						z_flag <= '0';
						n_flag <= '0';
					when "110" => --Shift right
						result <=  std_logic_vector(shift_right(unsigned(in0), to_integer(unsigned(in1))));
						z_flag <= '0';
						n_flag <= '0';
					when "111" => --TEST
						
						result <= "0000000000000000";
						if (in0 = "0000000000000000") then
							z_flag <= '1';
						else
							z_flag <= '0';
						end if;

						if (signed(in0) < 0 ) then
							n_flag <= '1';
						else
							n_flag <= '0';
						end if;


					when others =>
						NULL;
				end case;
				
				
			end if;
		end if;


	end process;



end Behavioral;
