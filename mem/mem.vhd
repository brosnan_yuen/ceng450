LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity mem is
	Generic (
		DATA_WIDTH	: integer := 16;
		addr_WIDTH	: integer := 16 
	);
	Port ( 
		clk 	: in  STD_LOGIC;
		rst 	: in  STD_LOGIC;
		wr_en	: in  STD_LOGIC;
		addr	: in  STD_LOGIC_VECTOR (addr_WIDTH - 1 downto 0);
		in_data : in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
		out_data: out STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0)
	);
end mem;

architecture Behavioral of mem is
	type Memory_Array is array (((2 ** (addr_WIDTH-5)) - 1) downto 0) of STD_LOGIC_VECTOR ((DATA_WIDTH - 1) downto 0); --2^11 addresses
	signal Memory : Memory_Array;
begin

	-- Read process
	process (clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				-- Clear out_data on rst
				out_data <= (others => '0');
			else 
				if wr_en = '1' then
					-- If wr_en then pass through DIn
					out_data <= (others => '0');
				else
					-- Otherwise Read Memory
					out_data <= Memory(to_integer(unsigned(addr(10 downto 0)))); -- truncating 16 bit address to 11 bits from LSB
				end if;
			end if;
		end if;
	end process;

	-- Write process
	process (clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				-- Clear Memory on rst
				for i in Memory'Range loop
					Memory(i) <= (others => '0');
				end loop;
			else
				if wr_en = '1' then
					-- Store in_data to Current Memory addr
					Memory(to_integer(unsigned(addr(10 downto 0)))) <= in_data;
				end if;
			end if;
		end if;
	end process;

end Behavioral;
