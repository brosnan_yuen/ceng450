library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library vunit_lib;
context vunit_lib.vunit_context;


entity tb_mem is
  generic (runner_cfg : string);
end entity;

architecture tb of tb_mem is



component mem is
	Port ( 
		clk 	: in  STD_LOGIC;
		rst 	: in  STD_LOGIC;
		wr_en	: in  STD_LOGIC;
		addr	: in  STD_LOGIC_VECTOR (15 downto 0);
		in_data : in  STD_LOGIC_VECTOR (15 downto 0);
		out_data: out STD_LOGIC_VECTOR (15 downto 0)
	);
end component;



signal  input_clk , input_rst, wr_en : std_logic;
signal  addr, in_data, out_data : std_logic_vector(15 downto 0);


begin
	mapping: mem port map(input_clk, input_rst, wr_en , addr, in_data, out_data );
	main : process
	begin
		test_runner_setup(runner, runner_cfg);
		report "Start testing MEM";
		
		
		input_clk <= '0';
		wr_en <= '0';	

		input_rst <= '1';
		addr <= (others => '0');
		in_data <= (others => '0');
			
		wait for 5 ns;
		input_clk <= '1';
		wait for 5 ns;
			
		check(out_data = "0000000000000000" , "Test reset output" );
		
		input_rst <= '0';
		
		input_clk <= '0';	
		wait for 5 ns;
		input_clk <= '1';
		wait for 5 ns;

		addr <= "0000000000000000";
		wr_en <= '1';
		in_data <= "1000000100010001";
		
		input_clk <= '0';	
		wait for 5 ns;
		input_clk <= '1';
		wait for 5 ns;

		
		addr <= "0000000000001001";
		wr_en <= '1';
		in_data <= "1000000000000100";
		
		input_clk <= '0';
		wait for 5 ns;
		input_clk <= '1';
		wait for 5 ns;



		addr <= "0000000000001101";
		wr_en <= '1';
		in_data <= "0000001000010000";
		
		input_clk <= '0';
		wait for 5 ns;
		input_clk <= '1';
		wait for 5 ns;


		addr <= "0000000000000000";
		wr_en <= '0';
		
		input_clk <= '0';
		wait for 5 ns;
		input_clk <= '1';
		wait for 5 ns;

		check(out_data =  "1000000100010001" , "Test output 1" );

		addr <= "0000000000001001";
		wr_en <= '0';
		
		input_clk <= '0';
		wait for 5 ns;
		input_clk <= '1';
		wait for 5 ns;


		check(out_data =  "1000000000000100" , "Test output 2" );

		addr <= "0000000000001101";
		wr_en <= '0';
		
		input_clk <= '0';
		wait for 5 ns;
		input_clk <= '1';
		wait for 5 ns;


		input_clk <= '0';
		wait for 5 ns;
		input_clk <= '1';
		wait for 5 ns;

		check(out_data = "0000001000010000" , "Test output 3" );


		test_runner_cleanup(runner); -- Simulation ends here
	end process;
end architecture;
