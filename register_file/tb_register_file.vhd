library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

use work.all; 
entity test_register_file is 
generic (runner_cfg : string);
end entity; 
architecture behavioural of test_register_file is 
component register_file port(rst : in std_logic; clk: in std_logic;
rd_index0, rd_index1: in std_logic_vector(2 downto 0); 
rd_data0, rd_data1: out std_logic_vector(15 downto 0); 
wr_index: in std_logic_vector(2 downto 0); 
wr_data: in std_logic_vector(15 downto 0); wr_enable: in std_logic); 
end component; 
signal rst, clk, wr_enable : std_logic; 
signal rd_index0, rd_index1, wr_index : std_logic_vector(2 downto 0); 
signal rd_data0, rd_data1, wr_data : std_logic_vector(15 downto 0);  
begin 
u0:register_file port map(rst, clk, rd_index0, rd_index1, rd_data0, rd_data1, wr_index, wr_data, wr_enable); 




process  
begin 

test_runner_setup(runner, runner_cfg);
report "Start testing Register file";


wr_data <= X"0000"; 
rst <= '1'; 
wr_index <= "000";
wr_enable <= '1'; 
wr_data <= X"200a";
rd_index1 <= "000"; 
rd_index0 <= "000"; 


clk <= '0'; 
wait for 10 us;
clk<='1';
wait for 10 us;


rst <= '0'; 
wr_enable <= '1';  

clk <= '0'; 
wait for 10 us; 
clk<='1'; 
wait for 10 us;


wr_index <= "001"; wr_data <= X"0037"; 

clk <= '0'; 
wait for 10 us; 
clk<='1'; 
wait for 10 us;

 wr_index <= "010"; wr_data <= X"8b00"; 

clk <= '0'; 
wait for 10 us; 
clk<='1'; 
wait for 10 us;

wr_index <= "101"; wr_data <= X"f00d"; 

clk <= '0'; 
wait for 10 us; 
clk<='1'; 
wait for 10 us;

wr_index <= "110"; wr_data <= X"00fd"; 

clk <= '0'; 
wait for 10 us; 
clk<='1'; 
wait for 10 us;

wr_index <= "111"; wr_data <= X"fd00"; 

clk <= '0'; 
wait for 10 us; 
clk<='1'; 
wait for 10 us;

wr_enable <= '0'; 

clk <= '0'; 
wait for 10 us; 
clk<='1'; 
wait for 10 us;

rd_index1 <= "001"; 
rd_index0 <= "010"; 

clk <= '0'; 
wait for 10 us; 
clk<='1'; 
wait for 10 us;

check(rd_data0 = X"8b00", "Test reset output" );
check(rd_data1 = X"0037", "Test reset output" );


rd_index1 <= "101"; 

clk <= '0'; 
wait for 10 us; 
clk<='1'; 
wait for 10 us;

check(rd_data1 = X"f00d", "Test reset output" );

rd_index0 <= "110"; 

clk <= '0'; 
wait for 10 us; 
clk<='1'; 
wait for 10 us;

check(rd_data0 = X"00fd", "Test reset output" );

rd_index1 <= "111";

clk <= '0'; 
wait for 10 us; 
clk<='1'; 
wait for 10 us;

check(rd_data1 = X"fd00", "Test reset output" );



test_runner_cleanup(runner); 

end process; 
end behavioural;
