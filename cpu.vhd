library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
 
entity cpu is
    Port ( 
		clk : in std_logic; 
		rst : in std_logic;
		in_data : in std_logic_vector(7 downto 0);
		out_data : out std_logic_vector(7 downto 0)
	);
end cpu;

architecture Behavioral of cpu is

--ROM
component ROM_VHDL is
    port(
         clk      : in  std_logic;
         addr     : in  std_logic_vector (6 downto 0);
         data     : out std_logic_vector (15 downto 0)
         );
end component;

--ROM signals
signal addr : std_logic_vector (6 downto 0);


--MEM
component mem is
	port(
		clk 	: in  STD_LOGIC;
		rst 	: in  STD_LOGIC;
		wr_en	: in  STD_LOGIC;
		addr	: in  STD_LOGIC_VECTOR (15 downto 0);
		in_data : in  STD_LOGIC_VECTOR (15 downto 0);
		out_data: out STD_LOGIC_VECTOR (15 downto 0)
		);
end component;

--MEM signals 
signal wrmem_en : std_logic;
signal mem_addr : std_logic_vector (15 downto 0);
signal mem_in, mem_out : std_logic_vector (15 downto 0);

--RF
component register_file is
port(rst : in std_logic; clk: in std_logic;
rd_index0, rd_index1: in std_logic_vector(2 downto 0); 
rd_data0, rd_data1: out std_logic_vector(15 downto 0); 
wr_index: in std_logic_vector(2 downto 0); 
wr_data: in std_logic_vector(15 downto 0); wr_enable: in std_logic);
end component;

--RF Signals
signal wr_enable : std_logic; 
signal rd_index0, rd_index1, wr_index : std_logic_vector(2 downto 0); 
signal rd_data0, rd_data1, wr_data : std_logic_vector(15 downto 0);


--ALU
component alu is
    port (
		clk : in std_logic; 
		rst : in std_logic;
		alu_mode : in std_logic_vector(2 downto 0);
		in0 : in std_logic_vector(15 downto 0);
		in1 : in std_logic_vector(15 downto 0);
		z_flag : out std_logic; 
		n_flag : out std_logic;
		result : out std_logic_vector(15 downto 0)
	);
end component;

--ALU signals
signal  alu_rst, alu_out_z ,alu_out_n : std_logic;
signal  alu_input_0, alu_input_1, alu_output_0 :std_logic_vector(15 downto 0);
signal  input_alu_mode :std_logic_vector(2 downto 0);


--Program counter
component pc is
	Port ( 
		clk 			: in  STD_LOGIC;
		rst 			: in  STD_LOGIC;
		increment		: in  STD_LOGIC;
		update			: in  STD_LOGIC;
		update_program_counter	: in  STD_LOGIC_VECTOR (15 downto 0);
		program_counter		: inout STD_LOGIC_VECTOR (15 downto 0)
	);
end component;

--Program counter signals
signal pc_increment, pc_update : std_logic;
signal update_program_counter, program_counter : std_logic_vector (15 downto 0) ;


--CPU Storage

--Current Instruction
signal instruction :  std_logic_vector (15 downto 0) ;


--Zero flag
signal z : std_logic := '0';

--Negative flag
signal n : std_logic := '0';


--FSM STATES

type state_type is (FETCH_STATE,DECODE_STATE, EMPTY_STATE,  WRITE_ALU_STATE , WRITE_N_Z_STATE ,  BR_SUB_PC_STATE , BR_SUB_ALU_STATE,  WRITE_PC_STATE, OUTPUT_RF_STATE, LOAD_STATE   );


--States

--Current state
signal current_state : state_type :=  FETCH_STATE;

--Next state
signal next_state : state_type :=  DECODE_STATE; 


--ALU INPUT 0 State
type alu_input_0_type is ( EMPTY , RF_INDEX_0, RF_INDEX_0_SHIFT , PCTOALU   );
signal alu_input_0_state : alu_input_0_type := EMPTY;

--ALU INPUT 1 State
type alu_input_1_type is ( EMPTY , RF_INDEX_1,  RF_INDEX_1_SHIFT_0 , RF_INDEX_1_SHIFT_1 , ALU_MASK );
signal alu_input_1_state : alu_input_1_type := EMPTY;

--MEM ADDR State
type mem_addr_type is (EMPTY, RF_INDEX_0 );
signal mem_addr_state : mem_addr_type := EMPTY;

--MEM IN State
type mem_in_type is (EMPTY, RF_INDEX_1 );
signal mem_in_state : mem_in_type := EMPTY;

--MEM WR_DATA State
type wr_data_type is ( EMPTY , INPUT, UPPER, LOWER , MEM_OUTPUT, ALU_OUTPUT , RF_INDEX_0 , PCTOWR );
signal wr_data_state : wr_data_type := EMPTY;

--Update program counter
type update_program_counter_type is ( EMPTY , ALUTOPC, RDTOPC );
signal update_program_counter_state : update_program_counter_type := EMPTY;


begin
	--Port Map
	alu0: alu port map(clk, alu_rst, input_alu_mode , alu_input_0, alu_input_1 , alu_out_z , alu_out_n  , alu_output_0 );
	rf0:  register_file port map(rst, clk, rd_index0, rd_index1, rd_data0, rd_data1, wr_index, wr_data, wr_enable);
	rom0: ROM_VHDL port map(clk, addr , instruction );
	mem0: mem port map(clk, rst, wrmem_en, mem_addr, mem_in, mem_out);
	pc0: pc port map(clk, rst, pc_increment, pc_update , update_program_counter, program_counter );
	
	process(clk,rst,current_state,instruction,in_data,addr, alu_input_0_state , alu_input_1_state, rd_data0 , rd_data1 , program_counter, n , z, next_state, alu_output_0, mem_out ,mem_addr_state ,mem_in_state , wr_data_state, update_program_counter_state  )
	begin
		--SYNC code
		if (rising_edge(clk)) then --Rising edge
			if (rst = '1') then --Reset
				input_alu_mode <= "000";
				z <= '0';
				n <= '0';
				current_state <= FETCH_STATE;
				next_state <= DECODE_STATE; 
				alu_rst <= '1';
				wr_enable <= '0';
				wrmem_en <= '0';
				addr <= (others => '0');
				out_data <= (others => '0');
				rd_index0 <= "000";
				rd_index1 <= "000";
				wr_index <= "000";
				wr_data_state <= EMPTY;
				mem_addr_state <= EMPTY;
				mem_in_state <= EMPTY;
				alu_input_0_state <= EMPTY;
				alu_input_1_state <= EMPTY;
				update_program_counter_state <= EMPTY;
				--pc_increment <= '0';
				pc_update <= '0';
				
			else --Next stage

				--Update state				
				current_state <= next_state;

				case next_state is
					when FETCH_STATE => --FETCH
						next_state <= DECODE_STATE;
						alu_rst <= '1';
						input_alu_mode <= "000";
						wr_enable <= '0';
						addr <= program_counter(7 downto 1);
						wrmem_en <= '0';
						rd_index0 <= "000";
						rd_index1 <= "000";
						wr_index <= "000";
						mem_addr_state <= EMPTY;
						mem_in_state <= EMPTY;
						wr_data_state <= EMPTY;
						alu_input_0_state <= EMPTY;
						alu_input_1_state <= EMPTY;
						update_program_counter_state <= EMPTY;
						--pc_increment <= '0';
						pc_update <= '0';


					when DECODE_STATE => --DECODE
						--if (instruction(15 downto 12) /= "1000") then --Update program counter
							--pc_increment <= '1';
						--end if;	

						case instruction(15 downto 9) is
							when "0000000" => --NOP
								next_state <= FETCH_STATE;
							when "0000111" => --TEST	
								next_state <= EMPTY_STATE;
								
								alu_input_0_state <= RF_INDEX_0;
								alu_input_1_state <= EMPTY;
								alu_rst <= '0';
								wr_enable <= '0';
								rd_index0 <= instruction(8 downto 6);
								rd_index1 <= "000";
								input_alu_mode <= instruction(11 downto 9);

							when "0100000" => --OUT
								next_state <= OUTPUT_RF_STATE;
								
								alu_rst <= '1';
								wr_enable <= '0';
								rd_index0 <= instruction(8 downto 6);
								rd_index1 <= "000";
							when "0100001" => --IN
								next_state <= EMPTY_STATE;
								
								alu_rst <= '1';
								wr_enable <= '1';
								wr_index <= instruction(8 downto 6);
								wr_data_state <= INPUT;
							
							when "1000000"=> --BRR
								next_state <= EMPTY_STATE;

								alu_rst <= '0';
								alu_input_0_state <= PCTOALU;
								
								alu_input_1_state <= RF_INDEX_1_SHIFT_0;
								input_alu_mode <= "001";

							when "1000001" => --BRR.N

								if (n = '1') then --N = 1
									input_alu_mode <= "001";
									alu_rst <= '0';
									
									alu_input_0_state <= PCTOALU;
									alu_input_1_state <= RF_INDEX_1_SHIFT_0;
									next_state <= EMPTY_STATE;
								else -- N = 0
									--pc_increment <= '1';
									next_state <= FETCH_STATE;
								end if;

							when "1000010" => --BRR.Z

								if (z = '1') then --Z = 1
									next_state <= EMPTY_STATE;

									input_alu_mode <= "001";
									alu_rst <= '0';
																
									alu_input_0_state <= PCTOALU;
									alu_input_1_state <= RF_INDEX_1_SHIFT_0;
								else -- Z = 0
									--pc_increment <= '1';
									next_state <= FETCH_STATE;
								end if;
								
							when "1000011" => --BR
								next_state <= EMPTY_STATE;
								
								alu_rst <= '0';
								alu_input_0_state <= RF_INDEX_0_SHIFT;
								input_alu_mode <= "001";
								wr_enable <= '0';
								rd_index0 <= instruction(8 downto 6);
								
								alu_input_1_state <= RF_INDEX_1_SHIFT_1;


							when "1000100" => --BR.N
								
								if (n = '1') then --N = 1
									next_state <= EMPTY_STATE;
									input_alu_mode <= "001";
									alu_rst <= '0';

									wr_enable <= '0';
									rd_index0 <= instruction(8 downto 6);
									alu_input_0_state <= RF_INDEX_0_SHIFT;
									
									alu_input_1_state <= RF_INDEX_1_SHIFT_1;
								else -- N = 0
									--pc_increment <= '1';
									next_state <= FETCH_STATE;
								end if;
								
							when "1000101" => --BR.Z
								
								if (z = '1') then --Z = 1
									next_state <= EMPTY_STATE;
									input_alu_mode <= "001";
									alu_rst <= '0';

									wr_enable <= '0';
									rd_index0 <= instruction(8 downto 6);
									alu_input_0_state <= RF_INDEX_0_SHIFT;
									
									alu_input_1_state <= RF_INDEX_1_SHIFT_1;
								else -- Z = 0
									--pc_increment <= '1';
									next_state <= FETCH_STATE;
								end if;
								
								
							
							when "1000110" => --BR.SUB
								next_state <= BR_SUB_PC_STATE;

								--pc_increment <= '1';
								wr_enable <= '1';
								wr_index <= "111";
								wr_data_state <= PCTOWR;
								
								
							when "1000111" => --RETURN
								next_state <=  WRITE_PC_STATE;
								
								wr_enable <= '0';
								rd_index0 <= "111";
								rd_index1 <= "000";
								
								update_program_counter_state <= RDTOPC;
								pc_update <= '1';
								
							when "0010000" => --LOAD
								wrmem_en <= '0';
								rd_index0 <= instruction(5 downto 3);
								wr_enable <= '0';
								mem_addr_state <= RF_INDEX_0;
								next_state <= LOAD_STATE;
								
																
							when "0010001" => --STORE
								rd_index0 <= instruction(5 downto 3); --source
								rd_index1 <= instruction(8 downto 6); --destination
								wr_enable <= '0';
								wrmem_en <= '1';
								mem_addr_state <= RF_INDEX_0;
								mem_in_state <= RF_INDEX_1;
								next_state <= EMPTY_STATE;
								
							when "0010010" => --LOADIMM
								wr_enable <= '1';
								wr_index <= "111";
								rd_index0 <= "111";
									if(instruction(8) = '1') then
									--uppper
									wr_data_state <= UPPER;
									else
									--lower
									wr_data_state <= LOWER;
									end if;
								next_state <= EMPTY_STATE;
							
							when "0010011" => --MOV
								rd_index0 <= instruction(5 downto 3); --source
								wr_index <= instruction(8 downto 6);
								wr_enable <= '1';
								wr_data_state <= RF_INDEX_0;
								next_state <= EMPTY_STATE;

							when others =>
								
								--ADD TO MUTLI
								if ((instruction(15 downto 9) = "0000001") or (instruction(15 downto 9) = "0000010")) or ((instruction(15 downto 9) = "0000011") or (instruction(15 downto 9) = "0000100"))	then --ALU
									alu_rst <= '0';
									alu_input_0_state <= RF_INDEX_0;
									alu_input_1_state <= RF_INDEX_1;
									next_state <= WRITE_ALU_STATE;
									
									wr_enable <= '0';
									rd_index0 <= instruction(5 downto 3);
									rd_index1 <= instruction(2 downto 0);
									input_alu_mode <= instruction(11 downto 9);
								end if;
								
								--SHL AND SHR
								if ((instruction(15 downto 9) = "0000101") or (instruction(15 downto 9) = "0000110"))  then --ALU
									alu_rst <= '0';
									alu_input_0_state <= RF_INDEX_0;
									alu_input_1_state <= ALU_MASK;
									next_state <= WRITE_ALU_STATE;
									
									wr_enable <= '0';
									rd_index0 <= instruction(8 downto 6);
									rd_index1 <= "000";
									input_alu_mode <= instruction(11 downto 9);
								end if;
								
								
						end case;
					
					when EMPTY_STATE => --NOTHING
						case  instruction(15 downto 9) is
							when "0000111" => --TEST 
								next_state <= WRITE_N_Z_STATE;
							when others =>	
								if (instruction(15 downto 12) = "1000") then --Branch instruction write program counter
									next_state <= WRITE_PC_STATE;
									
									update_program_counter_state <= ALUTOPC;
									pc_update <= '1';
								else --Other instructions
									next_state <= FETCH_STATE;
								end if;
						end case;

					when WRITE_ALU_STATE => --WRITE DATA FROM ALU TO RF
						wr_enable <= '1';
						wr_index <= instruction(8 downto 6);
						wr_data_state <= ALU_OUTPUT;
						next_state <= EMPTY_STATE;

					when WRITE_N_Z_STATE => --Write to N and Z flags
						z <= alu_out_z;
						n <= alu_out_n;
						next_state <= FETCH_STATE;
					

					when BR_SUB_PC_STATE => --BR.SUB compute next displacement
						next_state <= BR_SUB_ALU_STATE;
						wr_enable <= '0';
						rd_index0 <= instruction(8 downto 6);
					
						alu_input_1_state <= RF_INDEX_1_SHIFT_1;
						input_alu_mode <= "001";
						
						alu_rst <= '0';
						alu_input_0_state <= RF_INDEX_0_SHIFT;
						
						

					when BR_SUB_ALU_STATE => --BR.SUB wait for ALU
						next_state <= WRITE_PC_STATE;
						
						--pc_increment <= '0';
						update_program_counter_state <= ALUTOPC;
						pc_update <= '1';
						
					when WRITE_PC_STATE => --WRITE TO PC
						pc_update <= '0';
						update_program_counter_state <= EMPTY;
						
						next_state <= FETCH_STATE;

					when OUTPUT_RF_STATE  => --OUTPUT
						out_data <= rd_data0(7 downto 0);
						next_state <= FETCH_STATE;

					when LOAD_STATE =>
						wr_index <= instruction(8 downto 6);
						wr_enable <= '1';
						wr_data_state <= MEM_OUTPUT;
						
						next_state <= EMPTY_STATE;
						
					when others =>
						alu_rst <= '1';
						addr <= (others => '0');
						out_data <= (others => '0');
				end case;


				
			end if;
		end if;

		--ASYNC CODE
		
		--ALU INPUT 0 MUX
		case alu_input_0_state is
		      when EMPTY =>
		          alu_input_0 <= (others => '0');
		      when RF_INDEX_0 =>
		          alu_input_0 <= rd_data0;
		      when RF_INDEX_0_SHIFT =>
		          alu_input_0(0) <= '0';
                  alu_input_0(15 downto 1) <= rd_data0(15 downto 1) ;
		      when PCTOALU =>
		          alu_input_0 <= program_counter;
		      when others  =>
		          alu_input_0 <= (others => '0');
		end case;
		
		
		--ALU INPUT 1 MUX
		 --( EMPTY , RF_INDEX_1,  RF_INDEX_1_SHIFT_0 , RF_INDEX_1_SHIFT_1 , ALU_MASK );
        case alu_input_1_state is
              when EMPTY =>
                  alu_input_1 <= (others => '0');
              when RF_INDEX_1 =>
                  alu_input_1 <= rd_data1;
                  
              when RF_INDEX_1_SHIFT_0 =>
                  alu_input_1(0) <= '0';
                  alu_input_1(9 downto 1) <= instruction(8 downto 0);
                  alu_input_1(15 downto 10) <= (others => instruction(8));
                  
              when RF_INDEX_1_SHIFT_1 => 
                  alu_input_1(0) <= '0';
                  alu_input_1(6 downto 1) <= instruction(5 downto 0);
                  alu_input_1(15 downto 7) <= (others => instruction(5));
                  
              when ALU_MASK =>
                  alu_input_1 <= (15 downto 4 => '0') &  instruction(3 downto 0);
                  
              when others  =>
                  alu_input_1 <= (others => '0');
        end case;
		
		
		--MEM ADDR MUX
		case mem_addr_state is
              when EMPTY =>
                  mem_addr <= (others => '0');
              when RF_INDEX_0 =>
                   mem_addr <= rd_data0;
              when others  =>
                  mem_addr <= (others => '0');
        end case;
        
        --MEM IN MUX
        case mem_in_state is
              when EMPTY =>
                  mem_in <= (others => '0');
              when RF_INDEX_1 =>
                   mem_in <= rd_data1;
              when others  =>
                  mem_in <= (others => '0');
        end case;
        
        --WR DATA MUX
        --( EMPTY , INPUT, UPPER, LOWER , MEM_OUTPUT, ALU_OUTPUT , RF_INDEX_0  );
        case wr_data_state is
              when EMPTY =>
                  wr_data <= (others => '0');
              when INPUT =>
                  wr_data(15 downto 8) <= (others => '0');
                  wr_data(7 downto 0) <= in_data;
              when UPPER =>
                  wr_data(7 downto 0) <= rd_data0(7 downto 0);
                  wr_data(15 downto 8) <= instruction(7 downto 0);
              when LOWER =>
                  wr_data(7 downto 0) <= instruction(7 downto 0);
                  wr_data(15 downto 8) <= rd_data0(15 downto 8);
              when MEM_OUTPUT =>
                  wr_data <= mem_out;
                  
              when ALU_OUTPUT =>
                  wr_data <= alu_output_0;
                  
              when RF_INDEX_0 =>
                  wr_data <= rd_data0;

		when PCTOWR =>
			wr_data <= program_counter;

              when others  =>
                  wr_data <= (others => '0');
        end case;
		

	--UPDATE PROGRAM COUNTER MUX
	  case update_program_counter_state is
              when EMPTY =>
                  update_program_counter <= (others => '0');
		when ALUTOPC =>
			update_program_counter <=  alu_output_0;
		when RDTOPC =>
			update_program_counter <= rd_data0;
              when others  =>
                  update_program_counter <= (others => '0');
        end case;

	if (current_state = FETCH_STATE) and (rst = '0') then
		if (instruction(15 downto 12) /= "1000") then --Update program counter
			pc_increment <= '1';
		else
			case instruction(15 downto 9) is
				when "1000001" => --BRR.N

								if (n = '0') then --N = 0
									pc_increment <= '1';
								end if;

							when "1000010" => --BRR.Z

								if (z = '0') then --Z = 0
									pc_increment <= '1';
								end if;
								

							when "1000100" => --BR.N
								
								if (n = '0') then --N = 0
									pc_increment <= '1';
								end if;
								
							when "1000101" => --BR.Z
								
								if (z = '0') then --Z = 0
									pc_increment <= '1';
								end if;
								
								
							
							when "1000110" => --BR.SUB

								pc_increment <= '1';
							when others =>
								pc_increment <= '0';	
			end case;
		end if;

	else
		pc_increment <= '0';
	
	end if;

	
	end process;

end Behavioral;
